To build use:

```
cargo build --release
```

Then copy `target/release/jaccard_agreement.dll` in your working directory so Python will find it.
Change the ending to `.pyd`.
Use it via `import jaccard_agreement`.