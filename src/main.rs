use std::error::Error;
use std::fs::File;
use std::path::Path;

use bit_vec::BitVec;
use clap::{crate_authors, crate_name, crate_version, Arg, Command};
use csv::ReaderBuilder;

use jaccard_agreement::{jaccard_agreement, Cube, Cylinder, Neighborhood, Sphere};

fn main() -> Fallible {
    let matches = Command::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!(", "))
        .arg(
            Arg::new("LHS")
                .required(true)
                .help("Left hand side CSV file"),
        )
        .arg(
            Arg::new("RHS")
                .required(true)
                .help("Right hand side CSV file"),
        )
        .arg(
            Arg::new("FIELDS")
                .long("fields")
                .required(true)
                .min_values(1),
        )
        .arg(
            Arg::new("CELL_SIZES")
                .long("cell-sizes")
                .required(true)
                .min_values(1),
        )
        .arg(
            Arg::new("BUFFER_RADIUS")
                .long("buffer-radius")
                .default_value("5"),
        )
        .arg(
            Arg::new("NEIGHBORHOOD")
                .long("neighborhood")
                .default_value("sphere")
                .possible_values(&["cube", "sphere", "cylinder", "mixed"]),
        )
        .arg(
            Arg::new("UNCONSTRAINED_DIMENSIONS")
                .long("unconstrained-dimensions")
                .min_values(1),
        )
        .get_matches();

    let fields = matches.values_of("FIELDS").unwrap().collect::<Vec<_>>();

    let cell_sizes = matches
        .values_of("CELL_SIZES")
        .unwrap()
        .map(|arg| arg.parse())
        .collect::<Result<Vec<f64>, _>>()?;

    if fields.len() != cell_sizes.len() {
        return Err("Number of fields and cell sizes do not match.".into());
    }

    let buffer_radius = matches
        .value_of("BUFFER_RADIUS")
        .unwrap()
        .parse::<isize>()?;

    let lhs = Events::read(matches.value_of("LHS").unwrap(), &fields)?;
    let rhs = Events::read(matches.value_of("RHS").unwrap(), &fields)?;

    let val = match matches.value_of("NEIGHBORHOOD").unwrap() {
        "cube" => jaccard_agreement(lhs.iter(), rhs.iter(), &cell_sizes, &Cube, buffer_radius),
        "sphere" => jaccard_agreement(lhs.iter(), rhs.iter(), &cell_sizes, &Sphere, buffer_radius),
        "cylinder" => jaccard_agreement(
            lhs.iter(),
            rhs.iter(),
            &cell_sizes,
            &Cylinder,
            buffer_radius,
        ),
        "mixed" => {
            let unconstrained_dimensions = matches
                .values_of("UNCONSTRAINED_DIMENSIONS")
                .ok_or("Unconstrained dimensions defining the mixed neighborhood are missing.")?
                .map(|arg| arg.parse())
                .collect::<Result<Vec<usize>, _>>()?;

            if !unconstrained_dimensions
                .iter()
                .all(|dim| *dim < cell_sizes.len())
            {
                return Err(
                    "Unconstrained dimensions must be less than the number of fields.".into(),
                );
            }

            jaccard_agreement(
                lhs.iter(),
                rhs.iter(),
                &cell_sizes,
                &Mixed::new(unconstrained_dimensions, cell_sizes.len()),
                buffer_radius,
            )
        }
        _ => unreachable!(),
    };

    println!("{}", val);

    Ok(())
}

struct Events {
    len: usize,
    coords: Vec<f64>,
}

impl Events {
    fn read<P: AsRef<Path>>(path: P, fields: &[&str]) -> Fallible<Self> {
        let mut reader = ReaderBuilder::new()
            .delimiter(b';')
            .from_reader(File::open(path)?);

        let headers = reader.headers()?;

        let fields = fields
            .iter()
            .map(|field| {
                headers
                    .iter()
                    .position(|header| *field == header)
                    .ok_or_else(|| format!("Field {} is mssing in CSV file.", field))
            })
            .collect::<Result<Vec<_>, _>>()?;

        let mut coords = Vec::new();

        for record in reader.records() {
            let record = record?;

            coords.reserve(fields.len());

            for field in &fields {
                coords.push(
                    record
                        .get(*field)
                        .ok_or_else(|| format!("Field {} is missing in CSV record.", field))?
                        .parse()?,
                );
            }
        }

        Ok(Self {
            len: fields.len(),
            coords,
        })
    }

    fn iter(&self) -> impl Iterator<Item = impl Iterator<Item = f64> + '_> + Clone {
        assert!(self.coords.len() % self.len == 0);

        self.coords
            .chunks_exact(self.len)
            .map(|chunk| chunk.iter().copied())
    }
}

struct Mixed {
    unconstrained_dimensions: BitVec,
}

impl Mixed {
    fn new<I>(unconstrained_dimensions: I, len: usize) -> Self
    where
        I: IntoIterator<Item = usize>,
    {
        Self {
            unconstrained_dimensions: unconstrained_dimensions.into_iter().fold(
                BitVec::from_elem(len, false),
                |mut dims, dim| {
                    dims.set(dim, true);

                    dims
                },
            ),
        }
    }
}

impl Neighborhood for Mixed {
    const TRANSLATE: bool = true;

    fn contains(&self, indices0: &[isize], indices1: &[isize], buffer_radius: isize) -> bool {
        indices0
            .iter()
            .zip(indices1)
            .enumerate()
            .filter(|(dim, _)| !self.unconstrained_dimensions[*dim])
            .map(|(_, (index0, index1))| (*index0 - *index1).pow(2))
            .sum::<isize>()
            <= buffer_radius.pow(2)
    }
}

type Fallible<T = ()> = Result<T, Box<dyn Error>>;
