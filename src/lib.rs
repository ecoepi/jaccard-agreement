#![allow(clippy::needless_option_as_deref)]

use bit_vec::BitVec;

#[cfg(any(feature = "pyo3", test))]
use pyo3::{prelude::*, types::PyIterator};

#[cfg(any(feature = "pyo3", test))]
#[pymodule]
#[pyo3(name = "jaccard_agreement")]
fn module_wrapper(_py: Python, m: &PyModule) -> PyResult<()> {
    /// jaccard_agreement(
    ///     lhs,
    ///     rhs,
    ///     cell_sizes,
    ///     /,
    ///     neighborhood="sphere",
    ///     buffer_radius=5,
    /// )
    /// --
    ///
    /// Computes the Jaccard agreement between two sets of events `lhs` and `rhs`.
    ///
    /// `lhs` and `rhs` must be iterables with items that are themselves iterable which yield numbers, for example lists of tuples of numbers.
    ///
    /// `cell_sizes` should be a list of numbers. They are specified using the same units as the coordinate tuples defining the events.
    ///
    /// `neighborhood` can be one of "cube", "sphere" or "cylinder".
    ///
    /// `buffer_radius` is specified in cells.
    ///
    /// The first coordinate is assumed to be the unconstrained coordinate, e.g. the time, for the "cylinder" neighborhood, i.e. the events should be given as `(t, x, y, ...)`.
    #[pyfn(m)]
    #[pyo3(name = "jaccard_agreement")]
    fn function_wrapper(
        lhs: &PyAny,
        rhs: &PyAny,
        cell_sizes: Vec<f64>,
        neighborhood: Option<&str>,
        buffer_radius: Option<isize>,
    ) -> f64 {
        struct CoordsIter<'a>(&'a PyIterator);

        impl Iterator for CoordsIter<'_> {
            type Item = f64;

            fn next(&mut self) -> Option<Self::Item> {
                self.0
                    .next()
                    .map(|coord| coord.unwrap().extract::<f64>().unwrap())
            }
        }

        struct EventsIter<'a>(&'a PyIterator);

        impl<'a> Iterator for EventsIter<'a> {
            type Item = CoordsIter<'a>;

            fn next(&mut self) -> Option<Self::Item> {
                self.0
                    .next()
                    .map(|coords| CoordsIter(coords.unwrap().iter().unwrap()))
            }
        }

        #[derive(Clone)]
        struct Events<'a>(&'a PyAny);

        impl<'a> IntoIterator for Events<'a> {
            type Item = CoordsIter<'a>;
            type IntoIter = EventsIter<'a>;

            fn into_iter(self) -> Self::IntoIter {
                EventsIter(self.0.iter().unwrap())
            }
        }

        let lhs = Events(lhs);
        let rhs = Events(rhs);

        let buffer_radius = buffer_radius.unwrap_or(5);

        match neighborhood {
            None => jaccard_agreement(lhs, rhs, &cell_sizes, &Sphere, buffer_radius),
            Some("cube") => jaccard_agreement(lhs, rhs, &cell_sizes, &Cube, buffer_radius),
            Some("sphere") => jaccard_agreement(lhs, rhs, &cell_sizes, &Sphere, buffer_radius),
            Some("cylinder") => jaccard_agreement(lhs, rhs, &cell_sizes, &Cylinder, buffer_radius),
            Some(neighborhood) => panic!("Unknown neighborhood `{}`", neighborhood),
        }
    }

    Ok(())
}

pub fn jaccard_agreement<N, E, C>(
    lhs: E,
    rhs: E,
    cell_sizes: &[f64],
    neighborhood: &N,
    buffer_radius: isize,
) -> f64
where
    N: Neighborhood,
    E: IntoIterator<Item = C> + Clone,
    C: IntoIterator<Item = f64>,
{
    let bounds = Bounds::new(
        lhs.clone().into_iter(),
        rhs.clone().into_iter(),
        cell_sizes.len(),
    );
    let transform = Transform::new(&bounds, cell_sizes, buffer_radius);

    let lhs =
        Grid::new::<N, E::IntoIter, C>(lhs.into_iter(), &transform, neighborhood, buffer_radius);
    let rhs =
        Grid::new::<N, E::IntoIter, C>(rhs.into_iter(), &transform, neighborhood, buffer_radius);

    let mut intersection = lhs.clone();
    intersection.intersect(&rhs);
    let intersection = intersection.count();

    let mut union = lhs;
    union.unite(&rhs);
    let union = union.count();

    intersection as f64 / union as f64
}

#[derive(Clone)]
struct Grid(BitVec);

impl Grid {
    fn new<N, E, C>(
        events: E,
        transform: &Transform,
        neighborhood: &N,
        buffer_radius: isize,
    ) -> Self
    where
        N: Neighborhood,
        E: Iterator<Item = C>,
        C: IntoIterator<Item = f64>,
    {
        let mut grid = BitVec::from_elem(transform.extent() as usize, false);

        let len = transform.0.len();
        let mut indices0 = vec![0; len];
        let mut indices1 = vec![0; len];

        if N::TRANSLATE {
            let mut offsets = Vec::new();

            enumerate_neighborhood::<N, _>(
                len,
                &indices0,
                &mut indices1,
                neighborhood,
                buffer_radius,
                |indices| {
                    offsets.push(transform.flatten(indices));
                },
            );

            for coords in events {
                transform.apply(coords, &mut indices0);

                let index = transform.flatten(&indices0);

                for offset in &offsets {
                    grid.set((index + offset) as usize, true);
                }
            }
        } else {
            for coords in events {
                transform.apply(coords, &mut indices0);

                enumerate_neighborhood::<N, _>(
                    len,
                    &indices0,
                    &mut indices1,
                    neighborhood,
                    buffer_radius,
                    |indices| {
                        grid.set(transform.flatten(indices) as usize, true);
                    },
                );
            }
        }

        Self(grid)
    }

    fn intersect(&mut self, other: &Self) {
        self.0.and(&other.0);
    }

    fn unite(&mut self, other: &Self) {
        self.0.or(&other.0);
    }

    fn count(&self) -> u64 {
        self.0.blocks().map(|block| block.count_ones() as u64).sum()
    }
}

fn enumerate_neighborhood<N, F>(
    len: usize,
    indices0: &[isize],
    indices1: &mut [isize],
    neighborhood: &N,
    buffer_radius: isize,
    mut f: F,
) where
    N: Neighborhood,
    F: FnMut(&[isize]),
{
    for (index1, index0) in indices1.iter_mut().zip(indices0) {
        *index1 = index0 - buffer_radius;
    }

    let mut idx = 0;

    while idx != len {
        if neighborhood.contains(indices0, indices1, buffer_radius) {
            f(indices1);
        }

        idx = 0;

        while idx != len {
            let index0 = &indices0[idx];
            let index1 = &mut indices1[idx];

            *index1 += 1;

            if *index1 > *index0 + buffer_radius {
                *index1 = *index0 - buffer_radius;

                idx += 1;
            } else {
                break;
            }
        }
    }
}

struct Bounds(Vec<(f64, f64)>);

impl Bounds {
    fn new<E, C>(lhs: E, rhs: E, len: usize) -> Self
    where
        E: Iterator<Item = C>,
        C: IntoIterator<Item = f64>,
    {
        let mut bounds = (0..len)
            .map(|_| (f64::INFINITY, f64::NEG_INFINITY))
            .collect::<Vec<_>>();

        for coords in lhs.chain(rhs) {
            for (bounds, coord) in bounds.iter_mut().zip(coords.into_iter()) {
                bounds.0 = bounds.0.min(coord);
                bounds.1 = bounds.1.max(coord);
            }
        }

        Self(bounds)
    }
}

struct Transform(Vec<(f64, f64, isize)>);

impl Transform {
    fn new(bounds: &Bounds, cell_sizes: &[f64], buffer_radius: isize) -> Self {
        assert_eq!(bounds.0.len(), cell_sizes.len());

        let transform = bounds
            .0
            .iter()
            .zip(cell_sizes)
            .map(|(bounds, cell_size)| {
                let buffer_size = (buffer_radius + 1) as f64 * cell_size;

                let min = bounds.0 - buffer_size;
                let max = bounds.1 + buffer_size;

                let scale = cell_size.recip();
                let extent = ((max - min) * scale).ceil() as isize;

                (min, scale, extent)
            })
            .collect::<Vec<_>>();

        Self(transform)
    }

    fn apply<C>(&self, coords: C, indices: &mut [isize])
    where
        C: IntoIterator<Item = f64>,
    {
        debug_assert_eq!(indices.len(), self.0.len());

        for ((index, coord), transform) in indices.iter_mut().zip(coords).zip(&self.0) {
            *index = ((coord - transform.0) * transform.1) as isize;
        }
    }

    fn flatten(&self, indices: &[isize]) -> isize {
        debug_assert_eq!(indices.len(), self.0.len());

        self.0
            .iter()
            .zip(indices)
            .rev()
            .fold(0, |flat_index, (transform, index)| {
                flat_index * transform.2 + index
            })
    }

    fn extent(&self) -> isize {
        self.0.iter().map(|transform| transform.2).product()
    }
}

pub trait Neighborhood {
    const TRANSLATE: bool;
    fn contains(&self, indices0: &[isize], indices1: &[isize], buffer_radius: isize) -> bool;
}

pub struct Cube;

impl Neighborhood for Cube {
    const TRANSLATE: bool = true;

    fn contains(&self, _indices0: &[isize], _indices1: &[isize], _buffer_radius: isize) -> bool {
        true
    }
}

pub struct Sphere;

impl Neighborhood for Sphere {
    const TRANSLATE: bool = true;

    fn contains(&self, indices0: &[isize], indices1: &[isize], buffer_radius: isize) -> bool {
        debug_assert_eq!(indices0.len(), indices1.len());

        indices0
            .iter()
            .zip(indices1)
            .map(|(index0, index1)| (*index0 - *index1).pow(2))
            .sum::<isize>()
            <= buffer_radius.pow(2)
    }
}

pub struct Cylinder;

impl Neighborhood for Cylinder {
    const TRANSLATE: bool = true;

    fn contains(&self, indices0: &[isize], indices1: &[isize], buffer_radius: isize) -> bool {
        Sphere.contains(&indices0[1..], &indices1[1..], buffer_radius)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use pyo3::py_run;

    #[test]
    fn python_bindings() {
        Python::with_gil(|py| {
            let m = PyModule::new(py, "jaccard_agreement").unwrap();
            module_wrapper(py, m).unwrap();
            py_run!(
                py,
                m,
                r#"
                lhs = [(0, 0, 0), (10, 10, 10)]
                rhs = [(0, 0, 0), (10, 10, 10)]
                
                assert m.jaccard_agreement(lhs, rhs, (1, 1, 1)) == 1.0
            "#
            );
        });
    }
}
